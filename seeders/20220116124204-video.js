"use strict";
const video = require("../models/data/video.json");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const updateDate = video.video.map((data) => {
      return {
        idKategori: data.idKategori,
        judulVideo: data.judulVideo,
        deskripsi: data.deskripsi,
        link: data.link,
        interview: data.interview,
        highlight: data.highlight,
        isMovieNews: data.isMovieNews,
        isSerialNews: data.isSerialNews,
        createdAt: new Date(),
        updatedAt: new Date(),
      };
    });
    await queryInterface.bulkInsert("videos", updateDate, {});
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("videos", null, {});
  },
};
