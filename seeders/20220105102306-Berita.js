"use strict";
const dataBerita = require("../models/data/berita.json");
var moment = require("moment");
let idLocale = require("moment/locale/id");
moment.updateLocale("id", idLocale);
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const updateDate = dataBerita.berita.map((berita) => {
      return {
        idKategori: berita.idKategori,
        judulBerita: berita.judulBerita,
        sumberBerita: berita.sumberBerita,
        isMovieNews: berita.isMovieNews,
        isSerialNews: berita.isSerialNews,
        jumlahLihat: berita.jumlahLihat,
        jumlahLike: berita.jumlahLike,
        jumlahKomen: berita.jumlahKomen,
        pilihan: berita.pilihan,
        isiBerita: berita.isiBerita,
        sinopsis: berita.sinopsis,
        bannerBerita: berita.bannerBerita,
        soundtrack: berita.soundtrack,
        tanggalDibuat:
          berita.tanggalDibuat === ""
            ? moment(new Date()).format("DD MMM YYYY")
            : "",
        createdAt: moment(new Date()).format("DD MMM YYYY"),
        updatedAt: moment(new Date()).format("DD MMM YYYY"),
      };
    });
    await queryInterface.bulkInsert("Berita", updateDate, {});
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Berita", null, {});
  },
};
