"use strict";
const user = require("../models/data/user.json");
const bcrypt = require("bcryptjs");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const updateDate = user.user.map((data) => {
      return {
        fullname: data.fullname,
        email: data.email,
        password: bcrypt.hashSync(
          data.password,
          10 || Number(process.env.SALT)
        ),
        isAdmin: data.isAdmin,
        isSubscribe: data.isSubscribe,
        isLogin: data.isLogin,
        createdAt: new Date(),
        updatedAt: new Date(),
      };
    });
    await queryInterface.bulkInsert("users", updateDate, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("users", null, {});
  },
};
