var updatePostStats = {
  Like: function (beritaId) {
    document.getElementById("count-" + beritaId).textContent++;
  },
  Unlike: function (beritaId) {
    document.getElementById("count-" + beritaId).textContent--;
  },
};

var toggleButtonText = {
  Like: function () {
    document.getElementById("count").classList.remove("far fa-thumbs-up");
    document.getElementById("count").classList.add("fas fa-thumbs-up");
  },
  Unlike: function () {
    document.getElementById("count").classList.remove("fas fa-thumbs-up");
    document.getElementById("count").classList.add("far fa-thumbs-up");
  },
};

var actOnPost = function (event) {
  var postId = event.target.value;
  console.log(event);
  var judulBerita = document.getElementById("card-title").innerText;
  toggleButtonText(event.target);
  updatePostStats(postId);
  $.ajax({
    URL: `/${judulBerita}/like`,
    method: "POST",
    data: {
      idBerita: postId,
      idUser: localStorage.getItem("idUser"),
    },
  });
};

// $.ajax({
//   URL: `/admin/procut/edit/${id}`,
//   method: "PUT",
//   data: {
//     name: name,
//     imgURL: imgURL,
//     price: price,
//     category: category,
//   },
// }).done(function (data) {
//   console.log(data);
//   window.location.replace("/admin/product");
// });
