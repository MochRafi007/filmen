document.addEventListener("DOMContentLoaded", function (event) {
  var truncated = document.getElementsByClassName("text-body-film");
  var judulBerita = [];

  $(".title").map(function () {
    judul = $(this).text();
    id = this.id;
    judulBerita.push({
      id: id,
      judulBerita: judul.trim(),
    });
  });
  for (var i = 0; i < truncated.length; i++) {
    var t = truncated[i];
    // Remove the truncate class to get at the un-truncated width
    t.classList.remove("text-body-film");
    t.style.display = "inline-block";
    // w = un-truncated width
    var w = t.clientWidth;
    t.style.display = "";
    t.classList.add("text-body-film");
    // 250 corresponds to the width in the CSS
    if (w >= 250) {
      // Generate read more link
      var readMore = document.createElement("a");
      readMore.href = `/detailBerita/${judulBerita[i].judulBerita
        .replace(/\s+$/, "")
        .split(" ")
        .join("-")}`;
      readMore.innerText = "Baca Selengkapnya";
      readMore.classList.add("read-more");
      t.parentNode.insertBefore(readMore, t.nextSibling);
    }
  }
});
