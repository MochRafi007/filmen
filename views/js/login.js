$(document).ready(function () {
  //   if (localStorage.getItem("Token") == null) {
  //     window.location.replace("/login");
  //   }
  $("#submitButton").click(function (event) {
    event.preventDefault();
    const email = $("#email").val();
    const password = $("#pwd").val();
    $.ajax({
      URL: `/login`,
      method: "POST",
      data: {
        email: email,
        password: password,
      },
    }).done((data) => {
      localStorage.setItem("Token", data);
      console.log(data);
    });
  });
});
