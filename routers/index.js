module.exports = {
  routerLogin: require("./client/login"),
  routerRegister: require("./client/register"),
  routerHomePage: require("./client/homepage"),
  routerDetailBerita: require("./client/detailBerita"),
  routerHalamanVideo: require("./client/video"),
  routerSerialNews: require("./client/serialnews"),
  routerFeedback: require("./client/feedback(post)"),
  routerContactUs: require("./client/contactus"),
  routerMovieNews: require("./client/movieNews"),

  // Halaman Admin
  routerAdmin: require("./admin/admin"),
  routerAdminVideo: require("./admin/adminvideo"),
  routerAddBerita: require("./admin/addBerita"),
  routerAddVideo: require("./admin/addVideo"),
  routerLoginAdmin: require("./admin/adminLogin"),
};
