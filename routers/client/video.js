const router = require("express").Router();
const controllerHalamanVideo = require("../../controllers/client/controllerHalamanVideo");

// Get video
router.get("/", controllerHalamanVideo.getHalamanVideo);
router.get("/kategori/:kategori", controllerHalamanVideo.getDataByKategori);

module.exports = router;
