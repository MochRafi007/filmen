const router = require("express").Router();
const controllerMovieNews = require("../../controllers/client/controllerMovieNews");

// Get Detail Berita
router.get("/", controllerMovieNews.getMovieNews);
router.get("/kategori/:kategori", controllerMovieNews.getDataByKategori);

module.exports = router;
