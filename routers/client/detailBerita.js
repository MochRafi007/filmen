const router = require("express").Router();
const controllerDetailBerita = require("../../controllers/client/controllerDetailBerita");

// Get Detail Berita
router.get("/:judulBerita", controllerDetailBerita.getDetailBerita);
router.post("/:judulBerita", controllerDetailBerita.postKomen);
router.post("/:judulBerita/like");

module.exports = router;
