const router = require("express").Router();
const controllerFeedback = require("../../controllers/client/controllerFeedback");
const controllerFeedBack = require("../../controllers/client/controllerFeedBackpost");

// Get Detail Berita
router.get("/", controllerFeedback.getFeedback);
router.post("/", controllerFeedBack.getFeedBack);

module.exports = router;
