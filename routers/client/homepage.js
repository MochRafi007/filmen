const router = require("express").Router();
const controllerHomePage = require("../../controllers/client/controllerHomePage");

// Get halaman homepage
router.get("/", controllerHomePage.getHomePage);
router.get("/kategori/:kategori", controllerHomePage.getDataByKategori);
router.get("/search/:judul", controllerHomePage.getDataSearch);
router.post("/subscribe", controllerHomePage.postSubscribe);

module.exports = router;
