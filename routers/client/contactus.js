const router = require("express").Router();
const controllerContactUs = require("../../controllers/client/controllerContactUs");

// Get Detail Contact Us
router.get("/", controllerContactUs.getContactUs);

module.exports = router;
