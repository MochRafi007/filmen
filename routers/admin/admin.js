const router = require("express").Router();
const controllerAdmin = require("../../controllers/admin/controllerAdmin");

// Get halaman login
router.get("/", controllerAdmin.getAdmin);
router.get("/edit/:id", controllerAdmin.getBeritaById);
router.put("/edit/:id", controllerAdmin.updateBerita);
router.delete("/", controllerAdmin.deleteBerita);
// router.delete("/:id", controllerAdmin.postLogin);

module.exports = router;
