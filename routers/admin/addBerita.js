const router = require("express").Router();
const controllerAddBerita = require("../../controllers/admin/controllerAddBerita");

// Get halaman login
router.get("/", controllerAddBerita.getPageAdd);
router.post("/", controllerAddBerita.postBerita);
// router.post("/", controllerAdmin.postLogin);

module.exports = router;
