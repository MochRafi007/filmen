const router = require("express").Router();
const controllerAdminVideo = require("../../controllers/admin/controllerVideo");

// Get halaman login
router.get("/", controllerAdminVideo.getAdmin);
router.get("/edit/:id", controllerAdminVideo.getBeritaById);
router.put("/edit/:id", controllerAdminVideo.updateBerita);
router.delete("/", controllerAdminVideo.deleteBerita);
// router.delete("/:id", controllerAdmin.postLogin);

module.exports = router;
