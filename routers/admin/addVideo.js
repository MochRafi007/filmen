const router = require("express").Router();
const controllerAddVideo = require("../../controllers/admin/controllerAddVideo");

// Get halaman login
router.get("/", controllerAddVideo.getPageAdd);
router.post("/", controllerAddVideo.postVideo);

module.exports = router;
