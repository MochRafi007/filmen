const router = require("express").Router();
const controllerAdmin = require("../../controllers/admin/controllerLoginAdmin");

// Get halaman login
router.get("/", controllerAdmin.getAdmin);
router.post("/", controllerAdmin.postLogin);

module.exports = router;
