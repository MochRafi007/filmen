"use strict";
const { Model } = require("sequelize");
const bcryptjs = require("bcryptjs");
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user.hasMany(models.comment, {
        foreignKey: "idUser",
      });
      user.hasMany(models.like, {
        foreignKey: "idUser",
      });
    }
    static #encrypt = (password) => bcryptjs.hashSync(password, 10);

    static register = ({
      fullname,
      email,
      password,
      isAdmin,
      isSubscribe,
      isLogin,
    }) => {
      const encryptedPassword = this.#encrypt(password);

      return this.create({
        fullname,
        email,
        password: encryptedPassword,
        isAdmin,
        isSubscribe,
        isLogin,
      });
    };
  }

  user.init(
    {
      fullname: {
        type: DataTypes.STRING,
        customValidator(value) {
          if (value === null) {
            throw new Error("Nama tidak boleh kosong");
          }
        },
      },
      email: {
        type: DataTypes.STRING,
        customValidator(value) {
          if (value === false) {
            throw new Error("Format tidak benar, contoh benar : foo@gmail.com");
          }
        },
      },
      password: {
        type: DataTypes.STRING,
        customValidator(value) {
          if (value < 8) {
            throw new Error("Password harus lebih dari 8");
          }
        },
      },
      isAdmin: DataTypes.BOOLEAN,
      isSubscribe: DataTypes.BOOLEAN,
      isLogin: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "user",
    }
  );
  return user;
};
