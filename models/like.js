"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class like extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      like.belongsTo(models.Berita, {
        foreignKey: "idBerita",
      });
      like.belongsTo(models.user, {
        foreignKey: "idUser",
      });
    }
  }
  like.init(
    {
      idUser: DataTypes.INTEGER,
      idBerita: DataTypes.INTEGER,
      isLike: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "like",
    }
  );
  return like;
};
