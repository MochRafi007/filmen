"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class video extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      video.belongsTo(models.kategori, {
        foreignKey: "idKategori",
      });
    }
  }
  video.init(
    {
      idKategori: DataTypes.INTEGER,
      judulVideo: DataTypes.STRING,
      deskripsi: DataTypes.STRING(1000),
      link: DataTypes.STRING,
      interview: DataTypes.BOOLEAN,
      highlight: DataTypes.BOOLEAN,
      isMovieNews: DataTypes.BOOLEAN,
      isSerialNews: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "video",
    }
  );
  return video;
};
