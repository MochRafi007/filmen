
# Filmen
Filmen merupakan website berita yang menyajikan informasi-informasi terkait film, movies, dan serial. Filmen merupakan website yang bisa diakses kapanpun dan dimanapun. Filmen mempunyai visi untuk membantu masyarakat Indonesia dengan memberikan kemudahan untuk mengakses berita terkait film maupun serial.


## Requirement

Harus melakukan beberapa instalasi terlebih dahulu :
- PostgreSQL
- NodeJS
## Build
Install semua dependency :
```bash
  npm install
```

Tambahkan seeder terlebih dahulu sesuai urutan :

```bash
  npx sequelize-cli db:seed --seed 20220108053713-kategoris.js
  npx sequelize-cli db:seed --seed 20220116131651-user.js
  npx sequelize-cli db:seed --seed 20220105102306-Berita.js
  npx sequelize-cli db:seed --seed 20220116124204-video.js
```
Jalankan aplikasi filmen :
```bash
  npm start
```


## Authors

https://filmen.herokuapp.com/aboutus


## License

[MIT](https://choosealicense.com/licenses/mit/)

