"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("Berita", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      idKategori: {
        type: Sequelize.INTEGER,
      },
      judulBerita: {
        type: Sequelize.STRING,
      },
      sumberBerita: {
        type: Sequelize.STRING,
      },
      isMovieNews: {
        type: Sequelize.BOOLEAN,
      },
      isSerialNews: {
        type: Sequelize.BOOLEAN,
      },
      jumlahLihat: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      jumlahLike: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      jumlahKomen: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      pilihan: {
        type: Sequelize.BOOLEAN,
      },
      isiBerita: {
        type: Sequelize.STRING(20000),
      },
      sinopsis: {
        type: Sequelize.STRING(2000),
      },
      bannerBerita: {
        type: Sequelize.STRING(500),
      },
      soundtrack: {
        type: Sequelize.STRING(500),
      },
      tanggalDibuat: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("Berita");
  },
};
