"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("videos", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      idKategori: {
        type: Sequelize.INTEGER,
      },
      judulVideo: {
        type: Sequelize.STRING,
      },
      deskripsi: {
        type: Sequelize.STRING(1000),
      },
      link: {
        type: Sequelize.STRING,
      },
      interview: {
        type: Sequelize.BOOLEAN,
      },
      highlight: {
        type: Sequelize.BOOLEAN,
      },
      isMovieNews: {
        type: Sequelize.BOOLEAN,
      },
      isSerialNews: {
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("videos");
  },
};
