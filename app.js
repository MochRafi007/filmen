if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}
const express = require("express");
const flash = require("express-flash");
const session = require("express-session");

const {
  routerLogin,
  routerRegister,
  routerHomePage,
  routerDetailBerita,
  routerHalamanVideo,
  routerMovieNews,
  routerSerialNews,
  routerFeedback,
  routerContactUs,
  routerAdmin,
  routerLoginAdmin,
  routerAddBerita,
  routerAddVideo,
  routerAdminVideo,
} = require("./routers");
const { errDataNotFound } = require("./middleware/errorHandling");
const app = express();
const port = process.env.PORT || 3000;

// Middleware
app.use(express.json());
app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: true }));
app.set("views", ["views/pages", "views/admin/pages"]);
app.use(
  session({
    // Key we want to keep secret which will encrypt all of our information
    secret: "secret",
    // Should we resave our session variables if nothing has changes which we dont
    resave: false,
    // Save empty value if there is no vaue which we do not want to do
    saveUninitialized: false,
  })
);
app.use(express.static("views"));
app.use(flash());

//Halaman Homepage
app.use("/", routerHomePage, errDataNotFound);
//Halaman Login
app.use("/login", routerLogin, errDataNotFound);
//Halaman Register
app.use("/register", routerRegister, errDataNotFound);
//Halaman Detail Berita
app.use("/detailBerita", routerDetailBerita, errDataNotFound);
//Halaman video
app.use("/video", routerHalamanVideo, errDataNotFound);
//Halaman Movie News
app.use("/movienews", routerMovieNews, errDataNotFound);
//Halaman Serial News
app.use("/serialnews", routerSerialNews, errDataNotFound);
//Halaman Feedback
app.use("/feedback", routerFeedback, errDataNotFound);
//Halaman Contact Us
app.use("/contactus", routerContactUs, errDataNotFound);
//Halaman Contact Us
app.use(
  "/aboutus",
  (req, res) => {
    res.render("aboutUs.ejs");
  },
  errDataNotFound
);
// app.use("/feedback", routerFeedBack, errDataNotFound);

//Halaman Admin
app.use("/admin", routerAdmin, errDataNotFound);
//Halaman Login Admin
app.use("/admin/login", routerLoginAdmin, errDataNotFound);
//Halaman Tambah Berita Admin
app.use("/admin/addBerita", routerAddBerita, errDataNotFound);
//Halama Tabel Video
app.use("/admin/video", routerAdminVideo, errDataNotFound);
//Halaman Tambah Video Admin
app.use("/admin/video/addVideo", routerAddVideo, errDataNotFound);
// app.use("/adminupdate", routerAdminUpdate);
// //untuk hapus berita
// app.use("/hapus/:id", routerHAPUS);
// //untuk add video
// app.use("/adminvideo", routerAdminVideo);
//Bad Request 404
app.get("*", function (req, res) {
  res.render("error_page.ejs", {
    error: {
      message: req.url + "ini tidak tersedia bro",
      data: 404,
    },
    err404: "404 Tidak Ditemukan",
  });
});

app.listen(port, () => {
  console.log(`Selamat datang di Filmen : ${port}`);
});
