module.exports = {
  errDataNotFound(err, req, res, next) {
    // Kalo error datanya kosong
    if (err.tipeError === "dataKosong") {
      res.status(404).render("error_page.ejs", {
        error: {
          data: 404,
        },
        err404: "404 Tidak Ditemukan",
      });
    }
    // Login Handle Error
    else if (err.tipeError === "login") {
      if (err.roles === "admin") {
        res.render("login-admin.ejs", {
          message: "Email tidak terdaftar",
        });
      } else {
        res.render("login_page.ejs", {
          message: "Email tidak terdaftar",
        });
      }
    } else if (err.tipeError === "!password") {
      if (err.roles === "admin") {
        res.render("login-admin.ejs", {
          message: "Password Anda salah",
        });
      } else {
        res.render("login_page.ejs", {
          message: "Password Anda salah",
        });
      }
    } else if (err.tipeError === "!admin") {
      res.render("login-admin.ejs", {
        message: "Anda tidak terdaftar sebagai Admin",
      });
    }
    // Register Handle Error
    else if (err.tipeError === "!email") {
      res.render("register_page.ejs", {
        message: "Tidak dapat menggunakan Email, karena Email Sudah Terdaftar",
      });
    } else {
      console.log(err);
      res.status(500).render("error_page.ejs", {
        error: {
          data: 500,
        },
        err500: "500 Server Error",
      });
    }
  },
};
