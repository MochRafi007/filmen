const { video } = require("../../models");
//membuat artikel berita
function createVideo(req, res) {
  const { link, idKategori, judulVideo, deskripsi } = req.body;
  video
    .create({ link, idKategori, judulVideo, deskripsi })
    .then((creatVideo) => {
      res.redirect("/adminupdate");
    })
    .catch(function (err) {
      console.log(err);
    });
}
module.exports = { createVideo };
