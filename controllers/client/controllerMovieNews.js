const { Op } = require("sequelize");
const { Berita, kategori, video } = require("../../models");
module.exports = {
  // Menampilkan halaman movie news
  getMovieNews(req, res, next) {
    // Mengambil data berita berdasarkan movie news
    Berita.findAll({
      order: [["jumlahLihat", `DESC`]],
      where: {
        isMovieNews: true,
      },
    })
      .then(async (data) => {
        if (data) {
          const dataTrending = await Berita.findAll({
            order: [["jumlahKomen", `DESC`]],
            limit: 5,
            where: {
              pilihan: false,
            },
          });
          const dataVideoTeratas = await video.findAll({
            limit: 5,
          });
          res.render("movienews_page.ejs", {
            data,
            page: "movienews",
            dataTrending,
            dataVideoTeratas,
          });
        } else {
          throw { tipeError: "dataKosong" };
        }
      })
      .catch(function (err) {
        next(err);
      });
  },
  getDataByKategori(req, res, next) {
    Berita.findAll({
      where: { isMovieNews: true },
      include: {
        model: kategori,
        where: {
          [Op.and]: [{ namaKategori: req.params.kategori }],
        },
      },
    })
      .then(async (data) => {
        if (data) {
          const dataTrending = await Berita.findAll({
            order: [["jumlahKomen", `DESC`]],
            limit: 5,
          });
          const dataVideoTeratas = await video.findAll({
            limit: 5,
          });
          res.render("kategori_page.ejs", {
            data,
            pageNow: "movienews",
            page: req.params.kategori,
            dataTrending,
            dataVideoTeratas,
          });
        } else {
          throw { tipeError: "dataKosong" };
        }
      })
      .catch(function (err) {
        next(err);
      });
  },
};
