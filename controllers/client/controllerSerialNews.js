const { Op } = require("sequelize");
const { Berita, kategori, video } = require("../../models");
module.exports = {
  // Menampilkan halaman serial news
  getSerialNews(req, res, next) {
    // Mengambil data berita berdasarkan serial news
    Berita.findAll({
      order: [["jumlahLihat", `DESC`]],
      where: {
        isSerialNews: true,
      },
    })
      .then(async (data) => {
        if (data) {
          const dataTrending = await Berita.findAll({
            order: [["jumlahKomen", `DESC`]],
            limit: 5,
            where: {
              pilihan: false,
            },
          });
          const dataVideoTeratas = await video.findAll({
            limit: 5,
          });
          res.render("serialnews_page.ejs", {
            data,
            page: "serialnews",
            dataTrending,
            dataVideoTeratas,
          });
        } else {
          throw { tipeError: "dataKosong" };
        }
      })
      .catch(function (err) {
        next(err);
      });
  },
  getDataByKategori(req, res, next) {
    Berita.findAll({
      where: { isSerialNews: true },
      include: {
        model: kategori,
        where: {
          [Op.and]: [{ namaKategori: req.params.kategori }],
        },
      },
    })
      .then(async (data) => {
        if (data) {
          const dataTrending = await Berita.findAll({
            order: [["jumlahKomen", `DESC`]],
            limit: 5,
          });
          const dataVideoTeratas = await video.findAll({
            limit: 5,
          });
          res.render("kategori_page.ejs", {
            data,
            pageNow: "serialnews",
            page: req.params.kategori,
            dataTrending,
            dataVideoTeratas,
          });
        } else {
          throw { tipeError: "dataKosong" };
        }
      })
      .catch(function (err) {
        next(err);
      });
  },
};
