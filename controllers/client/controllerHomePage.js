const {
  Berita,
  comment,
  user,
  like,
  kategori,
  video,
} = require("../../models");
const sequelize = require("sequelize");
const { Op } = require("sequelize");
module.exports = {
  // Menampilkan halaman homepage
  getHomePage(req, res, next) {
    // Mengambil semua data berita
    Berita.findAll({
      order: [["jumlahLihat", `DESC`]],
      include: {
        model: kategori,
      },
      where: {
        pilihan: false,
      },
    })
      .then(async (data) => {
        if (data) {
          const dataUpdate = JSON.parse(JSON.stringify(data, null, 2));
          var dataAsia = [];
          var dataIndonesia = [];
          var dataBarat = [];
          const dataPilihan = await Berita.findAll({
            where: {
              pilihan: true,
            },
          });
          dataUpdate.map((berita, index) => {
            if (berita.kategori.namaKategori === "Asia" && index > 2) {
              dataAsia.push({
                idBerita: berita.id,
                judulBerita: berita.judulBerita,
                bannerBerita: berita.bannerBerita,
                type: berita.isMovieNews ? "Movie News" : "Serial News",
              });
            } else if (berita.kategori.namaKategori === "Barat" && index > 2) {
              dataBarat.push({
                idBerita: berita.id,
                judulBerita: berita.judulBerita,
                bannerBerita: berita.bannerBerita,
                type: berita.isMovieNews ? "Movie News" : "Serial News",
              });
            } else if (
              berita.kategori.namaKategori === "Indonesia" &&
              index > 2
            ) {
              dataIndonesia.push({
                idBerita: berita.id,
                judulBerita: berita.judulBerita,
                bannerBerita: berita.bannerBerita,
                type: berita.isMovieNews ? "Movie News" : "Serial News",
              });
            }
          });
          const dataTrending = await Berita.findAll({
            order: [["jumlahKomen", `DESC`]],
            limit: 5,
            where: {
              pilihan: false,
            },
          });
          const dataVideoTeratas = await video.findAll({
            limit: 5,
          });
          console.log(JSON.stringify(dataIndonesia, null, 2));
          res.render("home_page.ejs", {
            page: "home",
            data,
            dataIndonesia,
            dataBarat,
            dataAsia,
            dataPilihan,
            dataTrending,
            dataVideoTeratas,
          });
        } else {
          throw { tipeError: "dataKosong" };
        }
      })
      .catch(function (err) {
        next(err);
      });
  },
  getDataByKategori(req, res, next) {
    Berita.findAll({
      include: {
        model: kategori,
        where: {
          namaKategori: req.params.kategori,
        },
      },
    })
      .then(async (data) => {
        if (data) {
          const dataTrending = await Berita.findAll({
            order: [["jumlahKomen", `DESC`]],
            limit: 5,
          });
          const dataVideoTeratas = await video.findAll({
            limit: 5,
          });
          res.render("kategori_page.ejs", {
            data,
            page: req.params.kategori,
            pageNow: "home",
            dataTrending,
            dataVideoTeratas,
          });
        } else {
          throw { tipeError: "dataKosong" };
        }
      })
      .catch(function (err) {
        next(err);
      });
  },
  getDataSearch(req, res, next) {
    Berita.findAll({
      where: {
        judulBerita: {
          [Op.or]: {
            [Op.iLike]: `${req.params.judul}`,
            [Op.match]: sequelize.fn("to_tsquery", req.params.judul),
          },
        },
      },
    })
      .then(async (data) => {
        if (data) {
          const dataTrending = await Berita.findAll({
            order: [["jumlahKomen", `DESC`]],
            limit: 5,
            where: {
              pilihan: false,
              judulBerita: {
                [Op.or]: {
                  [Op.notILike]: `${req.params.judul}`,
                  [Op.notLike]: `%${req.params.judul}`,
                },
              },
            },
          });
          console.log(dataTrending);
          res.render("search_page.ejs", {
            data,
            search: req.params.judul,
            dataTrending,
          });
        } else {
          throw { tipeError: "dataKosong" };
        }
      })
      .catch(function (err) {
        next(err);
      });
  },
  postSubscribe(req, res, next) {
    console.log(req.body);
  },
};
