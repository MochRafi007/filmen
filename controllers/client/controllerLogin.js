const { user } = require("../../models");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
module.exports = {
  // Menampilkan halaman login
  getLogin(req, res, next) {
    res.render("login_page.ejs");
  },
  // Authentication login
  postLogin(req, res, next) {
    const { email, password } = req.body;
    let errorsEmail = [];
    let errorsPassword = [];
    if (!email) {
      errorsEmail.push({ message: "Mohon isi email Anda" });
    }

    if (!password) {
      errorsPassword.push({ message: "Mohon isi password Anda" });
    }

    if (errorsEmail.length > 0 || errorsPassword.length > 0) {
      res.render("login_page.ejs", {
        errorsEmail,
        errorsPassword,
        email,
        password,
      });
    } else {
      user
        .findOne({
          where: {
            email: email,
          },
        })
        .then(function (data) {
          // Kalo username nya ada generate password dan decrypt password menggunakan bcrypt
          if (data) {
            bcrypt.compare(
              req.body.password,
              data.password,
              function (err, result) {
                try {
                  // Kalo username ada dan password valid, maka berhasil login
                  if (result) {
                    const token = jwt.sign(data.toJSON(), "rahasia");
                    req.flash("token", `${token}`);
                    req.flash("idUser", `${data.id}`);
                    req.flash("fullname", `${data.fullname}`);
                    req.flash("email", `${data.email}`);
                    req.flash("isSubscribe", `${data.isSubscribe}`);
                    req.flash("sukses", `sukses_login`);
                    res.redirect("/");
                  }
                  // Kalo username ada dan password tidak valid, maka gagal login
                  else {
                    throw { tipeError: "!password" };
                  }
                } catch (error) {
                  next(error);
                }
              }
            );
          }
          // Error kalo email tidak terdaftar
          else {
            throw { tipeError: "login" };
          }
        })
        .catch(function (err) {
          next(err);
        });
    }
  },
};
