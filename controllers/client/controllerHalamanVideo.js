const { Berita, comment, user, kategori, video } = require("../../models");
const { Op } = require("sequelize");
const sequelize = require("sequelize");

module.exports = {
  getHalamanVideo(req, res, next) {
    video
      .findAll({})
      .then(async (data) => {
        const dataHighlight = await video.findAll({
          where: {
            highlight: true,
          },
        });
        const dataNoWayHome = await video.findAll({
          where: {
            [Op.or]: {
              judulVideo: {
                [Op.or]: {
                  [Op.iLike]: `${"nowayhome"}`,
                  [Op.match]: sequelize.fn("plainto_tsquery", "No Way Home"),
                },
              },
              deskripsi: {
                [Op.match]: sequelize.fn("plainto_tsquery", "No Way Home"),
              },
            },
          },
        });
        const dataLayanganPutus = await video.findAll({
          where: {
            judulVideo: {
              [Op.or]: {
                [Op.iLike]: `${"layanganputus"}`,
                [Op.match]: sequelize.fn("plainto_tsquery", "Layangan Putus"),
              },
            },
          },
        });
        const dataStartup = await video.findAll({
          where: {
            deskripsi: {
              [Op.or]: {
                [Op.iLike]: `${"startup"}`,
                [Op.match]: sequelize.fn("plainto_tsquery", "Start Up"),
              },
            },
          },
        });
        console.log(dataStartup);
        res.render("video_page.ejs", {
          page: "video",
          dataVideo: data,
          dataHighlight,
          dataNoWayHome,
          dataLayanganPutus,
          dataStartup,
        });
      })
      .catch((err) => {
        next(err);
      });
  },
  getDataByKategori(req, res, next) {
    video
      .findAll({
        include: {
          model: kategori,
          where: {
            namaKategori: req.params.kategori,
          },
        },
      })
      .then(async (data) => {
        if (data) {
          const dataTrending = await Berita.findAll({
            order: [["jumlahKomen", `DESC`]],
            limit: 5,
          });
          const dataVideoTeratas = await video.findAll({
            limit: 5,
          });
          res.render("kategori_page.ejs", {
            data,
            page: req.params.kategori,
            dataTrending,
            pageNow: "video",
            dataVideoTeratas,
          });
        } else {
          throw { tipeError: "dataKosong" };
        }
      })
      .catch(function (err) {
        next(err);
      });
  },
};
