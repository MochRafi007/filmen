const { feedback } = require("../../models");

// Menampilkan halaman feedback
function getFeedBack(req, res) {
  const { rating, alasan, pendapat } = req.body;
  feedback
    .create({ rating, alasan, pendapat })
    .then((getFeedback) => {
      res.redirect("/feedback");
    })
    .catch(function (err) {
      // next(err)
      console.log(err);
    });
}

module.exports = { getFeedBack };
