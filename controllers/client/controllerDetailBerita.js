const { Berita, comment, user, like } = require("../../models");
const { Op } = require("sequelize");
var moment = require("moment");
let idLocale = require("moment/locale/id");
moment.updateLocale("id", idLocale);

module.exports = {
  // Menampilkan halaman detail berita
  getDetailBerita(req, res, next) {
    // Mengambil data berdasarkan judul
    Berita.findOne({
      where: {
        judulBerita: req.params.judulBerita.split("-").join(" "),
      },
      include: {
        model: comment,
      },
    })
      .then(async (data) => {
        if (data) {
          const dataMovieNews = await Berita.findAll({
            where: {
              isMovieNews: true,
            },
          });
          const dataSerialNews = await Berita.findAll({
            where: {
              isSerialNews: true,
            },
          });
          const dataPilihan = await Berita.findAll({
            where: {
              pilihan: true,
            },
          });
          const dataKomen = await comment.findAll({
            where: {
              idBerita: data.id,
            },
            include: [
              {
                model: user,
              },
            ],
          });
          const dataTrending = await Berita.findAll({
            order: [["jumlahKomen", `DESC`]],
            limit: 5,
            where: {
              pilihan: false,
              id: {
                [Op.ne]: data.id,
              },
            },
          });
          const dataUpdateMovie = JSON.parse(
            JSON.stringify(dataMovieNews, null, 2)
          );
          const dataUpdateSerial = JSON.parse(
            JSON.stringify(dataSerialNews, null, 2)
          );
          const dataUpdateKomen = JSON.parse(JSON.stringify(dataKomen));
          res.render("detailBerita_page.ejs", {
            data,
            judul: req.params.judulBerita,
            dataUpdateMovie,
            dataUpdateSerial,
            dataPilihan,
            dataTrending,
            dataUpdateKomen,
          });
          Berita.increment(
            { jumlahLihat: 1 },
            { where: { judulBerita: data.judulBerita } }
          );
        } else {
          throw { tipeError: "dataKosong" };
        }
      })
      .catch(function (err) {
        next(err);
      });
  },
  postKomen(req, res, next) {
    const { idUser, idBerita, komen } = req.body;
    console.log(req.body);

    comment
      .create({
        idUser: idUser,
        idBerita: idBerita,
        isiKomen: komen,
        tanggalKomen: moment(new Date()).format("DD MMM YYYY"),
      })
      .then(async function (data) {
        await Berita.increment({ jumlahKomen: 1 }, { where: { id: idBerita } });
        res.status(200).send(JSON.stringify(data));
      })
      .catch(function (err) {
        next(err);
      });
  },
  postLike(req, res, next) {
    like
      .findOne({
        where: {
          idBerita: req.body.idBerita,
          idUser: req.body.idUser,
        },
      })
      .then((data) => {
        if (data) {
          if (data.isLike) {
            like.update({
              where: {
                id: data.id,
              },
            });
          } else {
          }
        } else {
          throw { tipeError: "dataKosong" };
        }
      });
  },
};
