const { user } = require("../../models");
const bcrypt = require("bcryptjs");
module.exports = {
  // Menampilkan halaman register
  getRegister(req, res) {
    res.render("register_page.ejs");
  },
  async postRegister(req, res, next) {
    const { fullname, email, password } = req.body;
    var regex = /\d+/g;
    let errorsName = [];
    let errorsEmail = [];
    let errorsPassword = [];

    if (!fullname) {
      errorsName.push({ message: "Mohon isi nama Anda" });
    }

    if (!email) {
      errorsEmail.push({ message: "Mohon isi email Anda" });
    }

    if (!password) {
      errorsPassword.push({ message: "Mohon isi password Anda" });
    }

    if (password.length < 8 && password.match(regex) === false) {
      errorsPassword.push({
        length: "Password minimal 8 karakter dan 1 angka",
      });
    }

    if (
      errorsName.length > 0 ||
      errorsEmail.length > 0 ||
      errorsPassword.length > 0
    ) {
      res.render("register_page.ejs", {
        errorsName,
        errorsEmail,
        errorsPassword,
        fullname,
        email,
        password,
      });
    } else {
      user
        .findOne({
          where: {
            email: email,
          },
        })
        .then((data, err) => {
          if (data) {
            throw { tipeError: "!email" };
          } else {
            user
              .create({
                fullname: fullname,
                email: email,
                password: bcrypt.hashSync(password, 10),
              })
              .then(function (data) {
                req.flash("success_msg", `Berhasil Mendaftarkan ${fullname}`);
                res.status(200).redirect("/login");
              })
              .catch(function (err) {
                next(err);
              });
          }
        })
        .catch(function (err) {
          next(err);
        });
    }
  },
};
