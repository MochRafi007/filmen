const { Berita, kategori } = require("../../models");

//menampilkan halaman admin update video
module.exports = {
  getAdmin(req, res, next) {
    Berita.findAll({
      include: {
        model: kategori,
      },
      order: [["tanggalDibuat", `ASC`]],
    })
      .then((berita) => {
        res.render("admin.ejs", { berita });
      })
      .catch((err) => {
        console.log(err);
      });
  },
  getBeritaById(req, res, next) {
    Berita.findOne({
      where: {
        id: req.params.id,
      },
    })
      .then((data) => {
        res.render("updateBerita.ejs", { data });
      })
      .catch((err) => {
        next(err);
      });
  },
  updateBerita(req, res, next) {
    const {
      idKategori,
      judulBerita,
      sumberBerita,
      tag,
      isiBerita,
      sinopsis,
      bannerBerita,
      soundtrack,
    } = req.body;
    console.log(req.body);
    let errorsJudul = [];
    let errorsSumber = [];
    let errorsBerita = [];
    let errorsSinopsis = [];
    let errorsGambar = [];

    if (!judulBerita) {
      errorsJudul.push({ message: "Mohon isi judul berita" });
    } else {
      errorsJudul.push({ message: "" });
    }

    if (!sumberBerita) {
      errorsSumber.push({ message: "Mohon isi sumber berita" });
    } else {
      errorsSumber.push({ message: "" });
    }

    if (isiBerita === "\n") {
      errorsBerita.push({ message: "Mohon isi berita" });
    } else {
      errorsBerita.push({ message: "" });
    }
    if (!sinopsis) {
      errorsSinopsis.push({ message: "Mohon isi sinopsis berita" });
    } else {
      errorsSinopsis.push({ message: "" });
    }
    if (!bannerBerita) {
      errorsGambar.push({ message: "Mohon isi gambar berita" });
    } else {
      errorsGambar.push({ message: "" });
    }
    if (
      errorsJudul[0].message.length > 0 ||
      errorsBerita[0].message.length > 0 ||
      errorsSinopsis[0].message.length > 0 ||
      errorsSumber[0].message.length > 0 ||
      errorsGambar[0].message.length > 0
    ) {
      res.status(200).json({
        tipe: "error",
        data: [
          errorsJudul,
          errorsSumber,
          errorsBerita,
          errorsSinopsis,
          errorsGambar,
        ],
      });
    } else {
      Berita.update(
        {
          idKategori,
          judulBerita,
          sumberBerita,
          isMovieNews: tag === "movie" ? true : false,
          isSerialNews: tag === "serial" ? true : false,
          isiBerita,
          sinopsis,
          bannerBerita,
          soundtrack,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      )
        .then((data) => {
          res.status(200).json({
            message: `Berhasil Update Data Berita ${judulBerita}`,
          });
        })
        .catch((err) => {
          next(err);
        });
    }
  },
  deleteBerita(req, res, next) {
    console.log(req.query);
    // Berita.destroy({
    //   where: {
    //     id: req.body.id,
    //   },
    // })
    //   .then((data) => {
    //     res
    //       .status(200)
    //       .json({ message: `Berhasil Menghapus Data ${data.judulBerita}` });
    //   })
    //   .catch((err) => {
    //     next(err);
    //   });
  },
};
