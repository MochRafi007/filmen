const { Berita, kategori, video } = require("../../models");

//menampilkan halaman admin update video
module.exports = {
  getAdmin(req, res, next) {
    video
      .findAll({
        include: {
          model: kategori,
        },
        order: [["createdAt", `ASC`]],
      })
      .then((video) => {
        res.render("adminVideo.ejs", { video });
      })
      .catch((err) => {
        console.log(err);
      });
  },
  getBeritaById(req, res, next) {
    video
      .findOne({
        where: {
          id: req.params.id,
        },
      })
      .then((data) => {
        res.render("updateVideo.ejs", { data });
      })
      .catch((err) => {
        next(err);
      });
  },
  updateBerita(req, res, next) {
    const { idKategori, judulVideo, linkVideo, deskripsi, tag } = req.body;
    console.log(req.body);
    let errorsJudul = [];
    let errorsDeskripsi = [];
    let errorsGambar = [];

    if (!judulVideo) {
      errorsJudul.push({ message: "Mohon isi judul berita" });
    } else {
      errorsJudul.push({ message: "" });
    }

    if (deskripsi === "\n") {
      errorsDeskripsi.push({ message: "Mohon isi berita" });
    } else {
      errorsDeskripsi.push({ message: "" });
    }

    if (!linkVideo) {
      errorsGambar.push({ message: "Mohon isi gambar berita" });
    } else {
      errorsGambar.push({ message: "" });
    }
    if (
      errorsJudul[0].message.length > 0 ||
      errorsDeskripsi[0].message.length > 0 ||
      errorsGambar[0].message.length > 0
    ) {
      res.status(200).json({
        tipe: "error",
        data: [errorsJudul, errorsDeskripsi, errorsGambar],
      });
    } else {
      video
        .update({
          idKategori,
          judulVideo,
          linkVideo,
          deskripsi,
          isMovieNews: tag === "movie" ? true : false,
          isSerialNews: tag === "serial" ? true : false,
          highlight: false,
          interview: false,
        })
        .then((createArtikel) => {
          res
            .status(200)
            .json({ message: `Berhasil Menambahkan Berita ${judulVideo}` });
        })
        .catch(function (err) {
          next(err);
        });
    }
  },
  deleteBerita(req, res, next) {
    console.log(req.query);
    // Berita.destroy({
    //   where: {
    //     id: req.body.id,
    //   },
    // })
    //   .then((data) => {
    //     res
    //       .status(200)
    //       .json({ message: `Berhasil Menghapus Data ${data.judulBerita}` });
    //   })
    //   .catch((err) => {
    //     next(err);
    //   });
  },
};
