const { user } = require("../../models");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
module.exports = {
  //menampilkan halaman admin
  getAdmin(req, res) {
    res.render("login-admin.ejs");
  },
  postLogin(req, res, next) {
    const { email, password } = req.body;
    let errorsEmail = [];
    let errorsPassword = [];
    console.log(req.body);
    if (!email) {
      errorsEmail.push({ message: "Mohon isi email Anda" });
    }

    if (!password) {
      errorsPassword.push({ message: "Mohon isi password Anda" });
    }

    if (errorsEmail.length > 0 || errorsPassword.length > 0) {
      res.render("login-admin.ejs", {
        errorsEmail,
        errorsPassword,
        email,
        password,
      });
      console.log(errorsEmail, errorsPassword);
    } else {
      user
        .findOne({
          where: {
            email: email,
          },
        })
        .then(async function (data) {
          // Kalo username nya ada generate password dan decrypt password menggunakan bcrypt
          if (data) {
            try {
              if (data.isAdmin) {
                bcrypt.compare(
                  req.body.password,
                  data.password,
                  function (err, result) {
                    try {
                      // Kalo username ada dan password valid, maka berhasil login
                      if (result) {
                        const token = jwt.sign(data.toJSON(), "rahasia");
                        req.flash("token", `${token}`);
                        req.flash("isAdmin", `${data.isAdmin}`);
                        req.flash("sukses", `sukses_login`);
                        res.redirect("/admin");
                      }
                      // Kalo username ada dan password tidak valid, maka gagal login
                      else {
                        throw { tipeError: "!password", roles: "admin" };
                      }
                    } catch (error) {
                      next(error);
                    }
                  }
                );
              } else {
                throw { tipeError: "!admin" };
              }
            } catch (error) {
              next(error);
            }
          }
          // Error kalo email tidak terdaftar
          else {
            throw { tipeError: "login", roles: "admin" };
          }
        })
        .catch(function (err) {
          next(err);
        });
    }
  },
};
