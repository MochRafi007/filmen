const { Berita, kategori } = require("../../models");
var moment = require("moment");
let idLocale = require("moment/locale/id");
moment.updateLocale("id", idLocale);
//menampilkan halaman admin update video
module.exports = {
  getPageAdd(req, res, next) {
    res.render("addBerita.ejs", { data: [] });
  },
  async postBerita(req, res, next) {
    const {
      idKategori,
      judulBerita,
      sumberBerita,
      tag,
      isiBerita,
      sinopsis,
      bannerBerita,
      soundtrack,
    } = req.body;
    console.log(req.body);
    let errorsJudul = [];
    let errorsSumber = [];
    let errorsBerita = [];
    let errorsSinopsis = [];
    let errorsGambar = [];

    if (!judulBerita) {
      errorsJudul.push({ message: "Mohon isi judul berita" });
    } else {
      errorsJudul.push({ message: "" });
    }

    if (!sumberBerita) {
      errorsSumber.push({ message: "Mohon isi sumber berita" });
    } else {
      errorsSumber.push({ message: "" });
    }

    if (isiBerita === "\n") {
      errorsBerita.push({ message: "Mohon isi berita" });
    } else {
      errorsBerita.push({ message: "" });
    }
    if (!sinopsis) {
      errorsSinopsis.push({ message: "Mohon isi sinopsis berita" });
    } else {
      errorsSinopsis.push({ message: "" });
    }
    if (!bannerBerita) {
      errorsGambar.push({ message: "Mohon isi gambar berita" });
    } else {
      errorsGambar.push({ message: "" });
    }
    if (
      errorsJudul[0].message.length > 0 ||
      errorsBerita[0].message.length > 0 ||
      errorsSinopsis[0].message.length > 0 ||
      errorsSumber[0].message.length > 0 ||
      errorsGambar[0].message.length > 0
    ) {
      res.status(200).json({
        tipe: "error",
        data: [
          errorsJudul,
          errorsSumber,
          errorsBerita,
          errorsSinopsis,
          errorsGambar,
        ],
      });
    } else {
      Berita.create({
        idKategori,
        judulBerita,
        sumberBerita,
        jumlahLike: 0,
        jumlahLihat: 0,
        jumlahKomen: 0,
        isMovieNews: tag === "movie" ? true : false,
        isSerialNews: tag === "serial" ? true : false,
        pilihan: false,
        isiBerita,
        sinopsis,
        bannerBerita,
        soundtrack,
        tanggalDibuat: moment(new Date()).format("DD MMM YYYY"),
      })
        .then((createArtikel) => {
          res
            .status(200)
            .json({ message: `Berhasil Menambahkan Berita ${judulBerita}` });
        })
        .catch(function (err) {
          console.log(err);
        });
    }
  },
};
