const { Berita, kategori, video } = require("../../models");
var moment = require("moment");
let idLocale = require("moment/locale/id");
moment.updateLocale("id", idLocale);
//menampilkan halaman admin update video
module.exports = {
  getPageAdd(req, res, next) {
    res.render("addVideo.ejs", { data: [] });
  },
  async postVideo(req, res, next) {
    const { idKategori, judulVideo, linkVideo, deskripsi, tag } = req.body;
    console.log(req.body);
    let errorsJudul = [];
    let errorsDeskripsi = [];
    let errorsGambar = [];

    if (!judulVideo) {
      errorsJudul.push({ message: "Mohon isi judul berita" });
    } else {
      errorsJudul.push({ message: "" });
    }

    if (deskripsi === "\n") {
      errorsDeskripsi.push({ message: "Mohon isi berita" });
    } else {
      errorsDeskripsi.push({ message: "" });
    }

    if (!linkVideo) {
      errorsGambar.push({ message: "Mohon isi gambar berita" });
    } else {
      errorsGambar.push({ message: "" });
    }
    if (
      errorsJudul[0].message.length > 0 ||
      errorsDeskripsi[0].message.length > 0 ||
      errorsGambar[0].message.length > 0
    ) {
      res.status(200).json({
        tipe: "error",
        data: [errorsJudul, errorsDeskripsi, errorsGambar],
      });
    } else {
      video
        .create({
          idKategori,
          judulVideo,
          linkVideo,
          deskripsi,
          isMovieNews: tag === "movie" ? true : false,
          isSerialNews: tag === "serial" ? true : false,
          highlight: false,
          interview: false,
        })
        .then((createArtikel) => {
          res
            .status(200)
            .json({ message: `Berhasil Menambahkan Berita ${judulVideo}` });
        })
        .catch(function (err) {
          next(err);
        });
    }
  },
};
